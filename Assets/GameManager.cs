using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private static int _coins;
    public static int Coins
    {
        get => _coins;
        set=>_coins = value;
    }

    private static int _totalCoins=10;
    public static int TotalCoins
    {
        get => _totalCoins;
    }
    // Start is called before the first frame update
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
        _coins= 0;
        

    }




    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Singleton Update");

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
