using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] Button _button;
    [SerializeField] TMP_Text _timeText;
    [SerializeField] TMP_Text _textCoins;

    // Start is called before the first frame update
    void Start()
    {
        _textCoins.text = GameManager.Coins + " / " + GameManager.TotalCoins;
    }

    // Update is called once per frame
    void Update()
    {
        //timer 
        int d = (int)(Time.timeSinceLevelLoad * 100.0f);
        int minutos = d / (60 * 100);
        int segundos = d % (60 * 100) / 100;
        int decimas = d % 100;
        _timeText.text = string.Format("{0:00}:{1:00}.{2:00}", minutos, segundos, decimas);

    }

    public void RecolectarCoins()
    {
        GameManager.Coins++;
        if (GameManager.Coins >= GameManager.TotalCoins)
            Final();

        _textCoins.text = GameManager.Coins + " / " + GameManager.TotalCoins;
    }

    void Final()
    {
        _button.gameObject.SetActive(true);

        Time.timeScale = 0;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        _button.onClick.AddListener(LoadScene);
    }

    public void LoadScene()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _button.gameObject.SetActive(false);
        Time.timeScale = 1;
        GameManager.Coins= 0;
        SceneManager.LoadScene(0);
    }

}
