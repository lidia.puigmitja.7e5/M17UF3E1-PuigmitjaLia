using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecolectarCoins : MonoBehaviour
{
    [SerializeField] UIController _uiController;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Coin"))
        {
            _uiController.RecolectarCoins();
            Destroy(other.gameObject);
        }
    }
}
